var functions = require('firebase-functions');
var nodemailer = require('nodemailer');


var responseHtml = '<!doctype html>\n' +
  '<html>\n' +
  '<head>\n' +
  '  <meta name="viewport" content="width=device-width">\n' +
  '  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n' +
  '  <title>Simple Transactional Email</title>\n' +
  '  <style>\n' +
  '    /* -------------------------------------\n' +
  '        INLINED WITH htmlemail.io/inline\n' +
  '    ------------------------------------- */\n' +
  '    /* -------------------------------------\n' +
  '        RESPONSIVE AND MOBILE FRIENDLY STYLES\n' +
  '    ------------------------------------- */\n' +
  '    @media only screen and (max-width: 620px) {\n' +
  '      table[class=body] h1 {\n' +
  '        font-size: 28px !important;\n' +
  '        margin-bottom: 10px !important;\n' +
  '      }\n' +
  '      table[class=body] p,\n' +
  '      table[class=body] ul,\n' +
  '      table[class=body] ol,\n' +
  '      table[class=body] td,\n' +
  '      table[class=body] span,\n' +
  '      table[class=body] a {\n' +
  '        font-size: 16px !important;\n' +
  '      }\n' +
  '      table[class=body] .wrapper,\n' +
  '      table[class=body] .article {\n' +
  '        padding: 10px !important;\n' +
  '      }\n' +
  '      table[class=body] .content {\n' +
  '        padding: 0 !important;\n' +
  '      }\n' +
  '      table[class=body] .container {\n' +
  '        padding: 0 !important;\n' +
  '        width: 100% !important;\n' +
  '      }\n' +
  '      table[class=body] .main {\n' +
  '        border-left-width: 0 !important;\n' +
  '        border-radius: 0 !important;\n' +
  '        border-right-width: 0 !important;\n' +
  '      }\n' +
  '      table[class=body] .btn table {\n' +
  '        width: 100% !important;\n' +
  '      }\n' +
  '      table[class=body] .btn a {\n' +
  '        width: 100% !important;\n' +
  '      }\n' +
  '      table[class=body] .img-responsive {\n' +
  '        height: auto !important;\n' +
  '        max-width: 100% !important;\n' +
  '        width: auto !important;\n' +
  '      }\n' +
  '    }\n' +
  '\n' +
  '    /* -------------------------------------\n' +
  '        PRESERVE THESE STYLES IN THE HEAD\n' +
  '    ------------------------------------- */\n' +
  '    @media all {\n' +
  '      .ExternalClass {\n' +
  '        width: 100%;\n' +
  '      }\n' +
  '      .ExternalClass,\n' +
  '      .ExternalClass p,\n' +
  '      .ExternalClass span,\n' +
  '      .ExternalClass font,\n' +
  '      .ExternalClass td,\n' +
  '      .ExternalClass div {\n' +
  '        line-height: 100%;\n' +
  '      }\n' +
  '      .apple-link a {\n' +
  '        color: inherit !important;\n' +
  '        font-family: inherit !important;\n' +
  '        font-size: inherit !important;\n' +
  '        font-weight: inherit !important;\n' +
  '        line-height: inherit !important;\n' +
  '        text-decoration: none !important;\n' +
  '      }\n' +
  '      .btn-primary table td:hover {\n' +
  '        background-color: #34495e !important;\n' +
  '      }\n' +
  '      .btn-primary a:hover {\n' +
  '        background-color: #34495e !important;\n' +
  '        border-color: #34495e !important;\n' +
  '      }\n' +
  '    }\n' +
  '  </style>\n' +
  '</head>\n' +
  '<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">\n' +
  '<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">\n' +
  '  <tr>\n' +
  '    <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>\n' +
  '    <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">\n' +
  '      <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">\n' +
  '\n' +
  '        <!-- START CENTERED WHITE CONTAINER -->\n' +
  '        <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Contact Via Portfolio.</span>\n' +
  '        <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">\n' +
  '\n' +
  '          <!-- START MAIN CONTENT AREA -->\n' +
  '          <tr>\n' +
  '            <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">\n' +
  '              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">\n' +
  '                <tr>\n' +
  '                  <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">\n' +
  '                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi </p>\n' +
  '                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">I have received your message and will respond as soon as possible</p>\n' +
  '                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Thank you</p>\n' +
  '                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Kemar Bell</p>\n' +
  '\n' +
  '                  </td>\n' +
  '                </tr>\n' +
  '              </table>\n' +
  '            </td>\n' +
  '          </tr>\n' +
  '\n' +
  '          <!-- END MAIN CONTENT AREA -->\n' +
  '        </table>\n' +
  '\n' +
  '        <!-- END CENTERED WHITE CONTAINER -->\n' +
  '      </div>\n' +
  '    </td>\n' +
  '    <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>\n' +
  '  </tr>\n' +
  '</table>\n' +
  '</body>\n' +
  '</html>\n';


exports.sendContactMessage = functions.database.ref('/contactMessages/{pushKey}').onWrite(function(event) {
  var snapshot = event.data;
  var gmailEmail = functions.config().gmail.email;
  var gmailPassword = functions.config().gmail.password;
  var config = 'smtps://' + gmailEmail + ':' + gmailPassword + '@smtp.gmail.com';

  var mailTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // secure:true for port 465, secure:false for port 587
    auth: {
      user: 'XXXXX@gmail.com',
      pass: 'XXXXX.'
    }
  });
  // Only send email for new messages.
  if (snapshot.previous.val() || !snapshot.val().name) {
    return false;
  }

  var val = snapshot.val();

  var mailOptions = {
    to: 'bellstpatrick@gmail.com',
    subject: 'Message from ' + val.name,
    html: val.html
  };

  var responseMailOptions = {
    to: val.email,
    from: 'bellstpatrick@gmail.com',
    subject: 'Contact Conformation',
    html: responseHtml
  };

  return mailTransport.sendMail(mailOptions).then(function ( ) {
    console.log('Mail forwarding complete: bellstpatrick@example.com');

    return mailTransport.sendMail(responseMailOptions).then(function () {
      return console.log('Response email sent to: ', val.email);
    });
  });

});
