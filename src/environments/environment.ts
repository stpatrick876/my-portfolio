// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyASmv5e0-V2diwSf-OrZnz53i3VG68Z6fo",
    authDomain: "portfolio-16d2d.firebaseapp.com",
    databaseURL: "https://portfolio-16d2d.firebaseio.com",
    projectId: "portfolio-16d2d",
    storageBucket: "portfolio-16d2d.appspot.com",
    messagingSenderId: "770250145098"
  }
};
