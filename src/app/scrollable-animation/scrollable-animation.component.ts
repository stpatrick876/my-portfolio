import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'portfolio-scrollable-animation',
  templateUrl: './scrollable-animation.component.html',
  styleUrls: ['./scrollable-animation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ScrollableAnimationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
