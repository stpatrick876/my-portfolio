import { Component, DoCheck, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { current } from 'codelyzer/util/syntaxKind';
interface Specialization {
  title: string;
  desc: string;
}
@Component({
  selector: 'portfolio-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit, DoCheck {
  current = 0;
  previous = 0;
   specializations: Specialization[] = [ {
     title: 'User Interface',
     desc: 'Fluent in vanilla js, jQuery, es6, Modern UI Frameworks (Angular, React, Vue ...)'
      },
     {
       title: 'Data Visualization',
       desc: 'Capable of transforming data into Rich charts and Diagrams'
     },
     {
       title: 'API',
       desc: 'Familiar with the Design,  Creation and Consumption of API REST/SOAP',
     },
     {
       title: 'SOA',
       desc: 'Experience working on applications using Service Oriented Architecture',
     },
     {
       title: 'Responsive Design',
       desc: 'Skilled in the combination of flexible grids, flexible images, and media queries to produce application that works well on any device'
     },
     {
       title: '508 Compliance',
       desc: 'familiar with creating accessible applications'
     }]
  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
     setInterval(() => {
       this.previous = this.current;
       if(this.current === this.specializations.length - 1){
         this.current = 0;
       } else {
         this.current++;
       }
     }, 10000);
  }

  ngDoCheck() {
    if(this.current !== this.previous) {
      this.renderer.removeClass(this.el.nativeElement.querySelector('.intro__specializations__description'), 'animated');
      this.renderer.addClass(this.el.nativeElement.querySelector('.intro__specializations__description'), 'animated');


    }
  }

}
