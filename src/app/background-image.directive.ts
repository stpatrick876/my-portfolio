import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[portfolioBackgroundImage]'
})
export class BackgroundImageDirective  implements OnInit{
 @Input('portfolioBackgroundImage') image;
  constructor(private el: ElementRef, private ren: Renderer2) {

  }
  ngOnInit() {
    const background  = `url("${this.image}")`
    this.ren.setStyle(this.el.nativeElement, 'background', background)
  }
}
