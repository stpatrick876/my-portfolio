import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2} from '@angular/core';
import Popper from 'popper.js';
import {Observable} from "rxjs/Observable";
import 'rxjs';
export interface Option {
  value: any;
  isSelected?: boolean;
}
@Component({
  selector: 'portfolio-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent implements OnInit {
  Options: Option[] = [];
  @Input('options')  optionsIn;
  @Output() selectionChange = new EventEmitter();
  selected: string[] = [];
  constructor(private el: ElementRef, private renderer: Renderer2, private ref: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.optionsIn.forEach(option => {
      this.Options.push({
        value: option
      });
    });
    const reference = this.el.nativeElement.querySelector('.selected-box');
    const optionsEl = this.el.nativeElement.querySelector('.options');
    const optionEl = this.el.nativeElement.querySelector('.option');

    optionsEl.toggle = () => {
      if(optionsEl.classList.contains('open')) {
        this.renderer.removeClass(optionsEl, 'open');
      } else {
        this.renderer.addClass(optionsEl, 'open');
      }
    };



    new Popper(reference, optionsEl, {
      placement: 'bottom-start',
      modifiers: {
        arrow: {
          enabled: true
        },
        hide: {
          fn: (data) => {

              return data;
          }
        }
      },
      onCreate: (data) => {
        // data is an object containing all the informations computed
        // by Popper.js and used to style the popper and its arrow
        // The complete description is available in Popper.js documentation

      },
      onUpdate: (data) => {
        // same as `onCreate` but called on subsequent updates

      }
    });
  }

  clearAll() {
    if (this.selected.length < 1) return false;
    this.selected = [];
    this.Options.forEach((option) => {
      option.isSelected = false;
    });
    this.selectionChange.emit( 'all');
  }

  toggleSelection(index: number, value?: string) {
    if (value) {
      this.selected.splice(this.selected.indexOf(value), 1);
      this.selectionChange.emit(value);
      return false;
    }
    const idx = this.selected.indexOf(this.Options[index].value);
    if(idx !== -1) {
      this.Options[index].isSelected = false;
      this.selected.splice(idx, 1);
    } else {
      this.Options[index].isSelected = true;
      this.selected.push(this.Options[index].value);
    }
    this.selectionChange.emit(this.Options[index].value);

  }

}
