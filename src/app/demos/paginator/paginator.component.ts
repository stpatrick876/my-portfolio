import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { PageData } from '../../common.interface';

@Component({
  selector: 'portfolio-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  btns: any[];
  paginationWrapper: Element;
  bigDotContainer: Element;
  littleDot: Element;
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() pageData: PageData;
  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.btns = this.el.nativeElement.querySelectorAll('.btn');
    this.paginationWrapper = this.el.nativeElement.querySelector('.pagination-wrapper');
    this.bigDotContainer = this.el.nativeElement.querySelector('.big-dot-container');
    this.littleDot = this.el.nativeElement.querySelector('.little-dot');
    this.btns.forEach((btn) => {
      this.renderer.listen(btn, 'click', (e) => {

        if (e.target.classList.contains('btn--prev')) {

          this.paginationWrapper.classList.add('transition-prev');
        } else {
          this.paginationWrapper.classList.add('transition-next');
        }

        setTimeout(() => {
          if(this.paginationWrapper.classList.contains('transition-next')) {
            this.paginationWrapper.classList.remove('transition-next');
          } else if(this.paginationWrapper.classList.contains('transition-prev')) {
            this.paginationWrapper.classList.remove('transition-prev');
          }
        }, 500);
      });
    });
  }

  changeAction(isNext: boolean, e: any) {
    if(!isNext && this.pageData.currentPage === 1 || isNext && this.pageData.currentPage === this.pageData.totalPages) {
      return false;
    }
    this.change.emit(isNext);
  }


}
