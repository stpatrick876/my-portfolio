import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Demo } from '../../common.interface';

@Component({
  selector: 'portfolio-demo-details',
  templateUrl: './demo-details.component.html',
  styleUrls: ['./demo-details.component.scss']
})
export class DemoDetailsComponent implements OnInit {
  @Input() demo: Demo;
  @Output() goBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  goBackAction() {

    this.goBack.emit(true);
  }

}
