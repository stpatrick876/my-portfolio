import { AfterViewInit, Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { Demo, PageData } from '../common.interface';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'portfolio-demos',
  templateUrl: './demos.component.html',
  styleUrls: ['./demos.component.scss']
})
export class DemosComponent implements OnInit {
  page = 1;
  demoPage: PageData;
  selectedDemo: Demo;

  demos: Demo[] = [{
    title: 'Pomodoro App',
    resources: ['Angular 2', 'RxJs', 'firebase', 'bootstrap 4', 'angular material', 'd3js', 'howlerjs', 'protractor', 'jasmine'],
    repoUrl: 'https://gitlab.com/stpatrick876/pomodoro',
    demoUrl: 'https://kb-pomodoro.surge.sh',
    description: 'Simple pomodoro app timer app built with Angular Framework'
  }, {
    title: 'Portfolio App',
    resources: ['Angular 2', 'RxJs', 'angular material', 'isotope.js', 'css3', 'firebase', 'popper.js'],
    repoUrl: 'https://gitlab.com/stpatrick876/my-portfolio.git',
    description: 'Portfolio app displaying my skills,'
  }, {
    title: 'Task Manager',
    resources: ['Angular 5', 'RxJs', 'expressJs, nestJs', 'bootstrap 4', 'angular material', 'auth0', 'isotope', 'ngrx (Redux)'],
    repoUrl: 'https://gitlab.com/kem-sou/simple-crud-app',
    description: 'Task management app with Authentication, Node api'
  }, {
    title: 'ngSpotify',
    resources: ['Angular 5', 'RxJs', 'angular material', 'skeleton,css', 'animate.css', 'css3', 'material icons', 'ngx-translate'],
    repoUrl: 'https://gitlab.com/stpatrick876/NgSpotify.git',
    description: 'Angular Application utilizing the spotify web api'
  }, {
    title: 'Business listing',
    resources: ['Angular 5', 'RxJs', 'Foundation css', 'ngx-translate', 'animate.css', 'google maps api'],
    repoUrl: 'https://gitlab.com/stpatrick876/businessListing.git',
    demoUrl: 'http://busines-listing.surge.sh/',
    description: 'Business listing application with Angular and Firebase',
    videoUrl: 'https://drive.google.com/file/d/1oYwPKSs0uH7ctjQuIyPmkIja_mQt50di/preview'

  },
    {
      title: 'Ng Io Chat',
      resources: ['Angular 2', 'RxJs', 'Materialize.css', 'express.js', 'socket.io', 'protractor', 'jasmine', 'firebase'],
      repoUrl: 'https://gitlab.com/stpatrick876/gitHubProfiler.git',
      demoUrl: 'http://earsplitting-bone.surge.sh/',
      videoUrl: 'https://drive.google.com/file/d/1NX5ERPjv9YFLf6gB3vIQ_r2q2xZPrkxK/preview',
      description: 'Real time chat application '
    },
    {
      title: 'Currency Converter',
      resources: ['React(es6)', 'Material-UI', 'scss', 'api.fixer.io'],
      repoUrl: 'https://gitlab.com/stpatrick876/currencyconverter',
      description: 'Currency conversion an comparisons',
      demoUrl: '',
      videoUrl: 'https://drive.google.com/file/d/1SO8jJ3sPZtLGmbCT4SXtSqmcA0aaLM2l/preview'
    },
    {
      title: 'D3js Showcase',
      resources: ['React(flow)', 'D3js', 'Material-UI', 'scss'],
      repoUrl: '',
      description: 'D3js examples',
      demoUrl: '',
      videoUrl: 'https://dummyimage.com/600x400/cccccc/0011ff&text=Coming+Soon'
    },
    {
      title: 'Email Client',
      resources: ['React(Typescript)', 'redux', 'Material-UI', 'less', 'mail gun api'],
      repoUrl: '',
      description: 'send, receive and track emails',
      demoUrl: '',
      videoUrl: 'https://dummyimage.com/600x400/cccccc/0011ff&text=Coming+Soon'
    },
    {
      title: 'Github Profiler',
      resources: ['Angular 2', 'RxJs', 'bootstrap 4', 'protractor', 'jasmine'],
      repoUrl: 'https://gitlab.com/stpatrick876/gitHubProfiler.git',
      demoUrl: 'http://earsplitting-bone.surge.sh/',
      videoUrl: '',
      description: 'Simple Angular application that utilizes the github api'
    },];


  constructor(public sanitizer: DomSanitizer) {}


  ngOnInit() {
    this.paginateData();
  }

  paginateData(page = 1) {
    this.demoPage = this.paginateArray(this.demos, page, 2);
  }
  paginateArray(collection, page = 1, numItems = 10): PageData {
    const currentPage = page;
    const perPage = numItems;
    const offset = (page - 1) * perPage;
    const paginatedItems = collection.slice(offset, offset + perPage);

    return {
      currentPage,
      perPage,
      total: collection.length,
      totalPages: Math.ceil(collection.length / perPage),
      data: paginatedItems
    };
  }

  onPageChangeAction(isNext) {
    if(isNext) {
      this.paginateData(this.demoPage.currentPage + 1);
    } else  {
      this.paginateData(this.demoPage.currentPage - 1);
    }
  }

  sanitizeVideoUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }





}
