import {
  AfterViewChecked, AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit,
  Renderer2
} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {Subject} from "rxjs/Subject";
import {NavService} from "./nav.service";
import { ANIMATIONS } from '../animations';
interface NavItem {
  anchor: string;
  text: string;
  content: string;
  isActive: boolean;
}
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  animations: [ANIMATIONS.toggleNavStateTrigger, ANIMATIONS.navItemStateTrigger]
})
export class NavigationComponent implements OnInit, AfterViewInit, AfterViewChecked {
  navState: string = 'navClosed';
   navItems: NavItem[];
   constructor(private _navSrvc: NavService, private elem: ElementRef, private renderer: Renderer2){}

  ngOnInit() {
    this.navItems = [{
      anchor: '/home',
      text: 'Home',
      isActive: true,
      content: 'the beginning'

    }, {
      anchor: '/about',
      content: 'Curious?',
      text: `About`,
      isActive: false

    }, {
      anchor: '/skills',
      content: 'I got game',
      text: 'Skills',
      isActive: false

    },{
      anchor: '/works',
      content: 'Only the finest',
      text: 'Work',
      isActive: false

    },{
      anchor: '/contact',
      content: 'Don"t hesitate',
      text: 'Contact',
      isActive: false

    }];
    this._navSrvc.navToggleEventAnnounced$.subscribe((isOpen) => {
      if (isOpen) {
        this.navState = 'navOpened';
      } else {
        this.navState = 'navClosed';
      }
    })

  }
  ngAfterViewInit(): void {
    (<any>$('.paper-text')).lettering();
  }
  ngAfterViewChecked() {
    (<any>$('.paper-text')).lettering();
  }
  private setActiveLink(activeItem: NavItem){
    this.navItems.forEach(item => {
      item.isActive = activeItem.anchor === item.anchor;
    });
  }

  openNav($event) {
    if ($event.toState === 'navOpened'){
      this.renderer.setStyle(this.elem.nativeElement.querySelector('.overlay-navigation'), 'width', '100%');
    }
  }
  closeNav($event) {
    if ($event.toState === 'navClosed') {
      this.renderer.setStyle(this.elem.nativeElement.querySelector('.overlay-navigation'), 'width', 0);
    } else if ($event.toState === 'navOpened'){
      this.renderer.setStyle(this.elem.nativeElement.querySelector('.overlay-navigation'), 'width', '100%');

    }

  }






}
