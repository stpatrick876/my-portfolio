import {Component, ElementRef, HostListener, Inject, OnInit, Renderer2} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {Subject} from "rxjs/Subject";
import {NavService} from "./nav.service";

interface NavItem {
  anchor: string;
  text: string;
  isActive: boolean;
}
@Component({
  selector: 'portfolio-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav-large.scss', './nav-small.scss']
})
export class NavComponent implements OnInit {
   navOpen: boolean = false;
   navItems: NavItem[];
   constructor(private _navSrvc: NavService){}

  ngOnInit() {
    this.navItems = [{
      anchor: '#intro',
      text: 'Intro',
      isActive: true

    }, {
      anchor: '#about',
      text: ` About`,
      isActive: false

    }, {
      anchor: '#skills',
      text: 'Skills',
      isActive: false

    },{
      anchor: '#resume',
      text: 'Resume',
      isActive: false

    },{
      anchor: '#workExample',
      text: 'Work',
      isActive: false

    },{
      anchor: '#contactMe',
      text: 'Contact',
      isActive: false
    }];
  }

  private setActiveLink(activeItem: NavItem){
    this.navItems.forEach(item => {
      item.isActive = activeItem.anchor === item.anchor;
    });
  }




  onPageScrollFinish(e: any, item: NavItem) {
    this.setActiveLink(item);
    this._navSrvc.notifyscrollFinishEventEvent(item.anchor);
  }

}
