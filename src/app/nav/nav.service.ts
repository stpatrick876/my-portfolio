import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";

@Injectable()
export class NavService {
  navOpen = true;
  private scrollFinishEvent: Subject<any> = new Subject<any>();
  public scrollFinishEventAnnounced$ = this.scrollFinishEvent.asObservable();

  private navToggleEvent: Subject<boolean> = new Subject<boolean>();
  public navToggleEventAnnounced$ = this.navToggleEvent.asObservable();

  constructor() { }


  notifyscrollFinishEventEvent(srolledTo: any) {
    this.scrollFinishEvent.next(srolledTo);
  }
  notifyNavToggleEvent() {
    console.log(this.navOpen)
    this.navOpen = !this.navOpen;
    this.navToggleEvent.next(this.navOpen);
  }
}
