import {
  animate, stagger, state, style, transition, trigger, query, keyframes,
  animateChild, group
} from '@angular/animations';

export const ANIMATIONS = {
  toggleNavStateTrigger : trigger('toggleNavState',  [
    transition('navClosed => navOpened', [

      query('nav ul li', stagger('100ms', [
        style({ opacity: 0, transform: 'translateX(-100%)'}),
        animate('140ms 10ms ease', keyframes([
          style({opacity: 0.1, transform: 'translateX(-100%)', offset: 0}),
          style({opacity: .5, transform: 'translateX(-50%)', offset: 0.3}),
          style({opacity: 1, transform: 'translateX(0)', offset: 1.0}),
        ]))]), {optional: true}),

       query('@navItemState', animateChild(), {optional: true})
    ])
    ,
    transition('navOpened => navClosed', [


      query('nav ul li', stagger('100ms', [
        animate('140ms 10ms ease', keyframes([
          style({opacity: 1, transform: 'translateX(0%)', offset: 0}),
          style({opacity: .5, transform: 'translateX(50%)',  offset: 0.3}),
          style({opacity: 0, transform: 'translateX(100%)', display: 'none',  offset: 1.0}),
        ]))]), {optional: true})
    ])
  ]),
  navItemStateTrigger : trigger('navItemState', [
    transition('navClosed => navOpened', [
      style({ opacity: 0 }),
      animate('1s', style({ opacity:1 }))
    ])

  ]),
  ROUTES: trigger('routeAnimation', [
    transition('* => homePage', [
      group([
        query(':enter', [
          style({ opacity: 0 }),
          animate('2s', style({ opacity: 1  }))
        ],{ optional: true }),
        query(':leave', [
          animate('1s', style({ opacity: 0 }))
        ],{ optional: true })
      ])
    ]),

    transition('* => aboutPage', [
      group([
        query(':enter', [
          style({ opacity: 0 }),
          animate('4s', style({ opacity: 1  }))
        ],{ optional: true }),
        query(':leave', [
          animate('1s', style({ opacity: 0 }))
        ],{ optional: true })
      ])
    ])
  ])
};
