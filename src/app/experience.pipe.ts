import { Pipe, PipeTransform } from '@angular/core';
import {APPROXIMATION} from "./skills/skills.component";

@Pipe({
  name: 'experience'
})
export class ExperiencePipe implements PipeTransform {

  transform(exp: any, args?: any): any {
    let suffix = '';
    if(exp.proxy === APPROXIMATION.PLUS) {
      suffix = '+';
    } else if(exp.proxy === APPROXIMATION.MINUS) {
      suffix = '-';
    }

    return `${exp.val}yrs${suffix}`;
  }

}
