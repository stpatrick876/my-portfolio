import { AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit, Renderer2 } from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {NavService} from "./nav/nav.service";
import { ActivatedRoute, NavigationEnd, Route, Router } from "@angular/router";
import {PageScrollInstance, PageScrollService} from "ng2-page-scroll";
import { ANIMATIONS } from './animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ ANIMATIONS.ROUTES ]
})
export class AppComponent implements OnInit, AfterViewInit {
  pageTitle: string;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private hostElement: ElementRef,
    public _navSrvc: NavService,
    private router: Router,
    private pageScrollService: PageScrollService,
    private activatedRoute: ActivatedRoute) {


  }

  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(route => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe((event) => this.pageTitle = event['title']);
  }
  ngAfterViewInit(): void {
    (<any>$('.flag')).lettering();
  }
  componentAdded($event){
    this._navSrvc.notifyNavToggleEvent();
   // this.pageTitle = this.route.snapshot.data[0]['title'];
  }

  prepRouteState(outlet: any) {
    //console.log(outlet.activatedRouteData['animation'])
    return outlet.activatedRouteData['animation'];
  }


}
