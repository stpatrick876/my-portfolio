export interface Demo {
  title: string;
  screenShot?: string;
  repoUrl?: string;
  demoUrl?: string;
  videoUrl?: string;
  poster?: string;
  resources?: string[];
  description: string;
}

export interface PageData {
  currentPage: number;
  perPage: number;
  total: number;
  totalPages: number;
  data: Demo[];
}
