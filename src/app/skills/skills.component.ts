import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {NavService} from "../nav/nav.service";
declare const Isotope: any;
 export enum  TYPE_COLOR {
   blue = 1,
   green,
   red,
   grey,
   purple,
   brown,
   orange,
   cyan,
   yellow,
   aqua,
   Bisque

 }
export enum TYPE {
  UI = 1,
  Backend,
  IDE,
  API,
  Source_Control,
  Automation,
  Testing,
  OS,
  Communication,
  Package_Management,
  Database
}
export enum APPROXIMATION{
  PLUS,
  MINUS
}
export namespace Types {

  export function keys(): Array<string>{
    const keys = Object.keys(TYPE);
    return keys.slice(keys.length / 2, keys.length);
  }
}
export interface Skill {
  name: string;
  exp?: {
    val: number;
    proxy?: APPROXIMATION;
     };
  icon?: string;
  type?: TYPE[];
}
@Component({
  selector: 'portfolio-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  skills: Skill[];
  types = Types;
  TYPE_COLOR = TYPE_COLOR;
  filterSet: any[] = [];
  currentSort: string;
  constructor(private elem: ElementRef, private renderer: Renderer2, private _navSrvc: NavService) { }
  get typeColorKeys() : Array<string> {
    const keys = Object.keys(this.TYPE_COLOR);
    console.log('keys colors', keys)

    return keys.slice(keys.length / 2);
  }
  get typeKeys() : Array<string> {
    const keys = Object.keys(this.types);
    console.log('keys ', keys)
    return keys.slice(keys.length / 2);
  }
  ngOnInit() {
    console.log(this.types)
    this.skills = [{
      name: 'html 5',
      exp: {
        val: 5,
        proxy: APPROXIMATION.PLUS
      },
      icon: 'devicon-html5-plain colored',
      type: [TYPE.UI ]

    }, {
      name: 'javascript',
      exp: {
        val: 4,
        proxy: APPROXIMATION.PLUS
      },
      icon: 'devicon-javascript-plain colored',
      type: [TYPE.UI ]

    }, {
      name: 'css',
      exp: {
        val: 4,
        proxy: APPROXIMATION.PLUS
      },
      icon: 'devicon-css3-plain colored',
      type: [TYPE.UI ]

    }, {
      name: 'jquery',
      exp: {
        val: 3,
        proxy: APPROXIMATION.PLUS
      },
      icon: 'devicon-jquery-plain colored',
      type: [TYPE.UI ]

    }, {
      name: 'java',
      exp: {
        val: 3,
        proxy: APPROXIMATION.PLUS
      },
      icon: 'devicon-java-plain colored',
      type: [TYPE.Backend ]
    },
      {
        name: 'Angular ',
        type: [TYPE.UI],
        exp: {
          val: 2,
          proxy: APPROXIMATION.PLUS
        },
        icon: 'devicon-angularjs-plain colored'
      },{
        name: 'Atom ',
        icon: 'devicon-atom-original colored',
        type: [TYPE.IDE]
      },{
        name: 'Bitbucket',
        icon: 'devicon-bitbucket-plain colored',
        type: [TYPE.Source_Control],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'Bootstrap',
        icon: 'devicon-bootstrap-plain colored',
        exp: {
          val: 3,
          proxy: APPROXIMATION.MINUS
        },
        type: [TYPE.UI]
      }, {
        name: 'git',
        icon: 'devicon-git-plain colored',
        type: [TYPE.Source_Control],
        exp: {
          val: 2,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'gulp',
        icon: 'devicon-gulp-plain colored',
        type: [TYPE.Automation],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'grunt',
        icon: 'devicon-grunt-line colored',
        type: [TYPE.Automation],
        exp: {
          val: 2,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'Webstorm',
        icon: 'devicon-webstorm-plain colored',
        type: [TYPE.IDE],
        exp: {
          val: 2,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'oracle',
        icon: 'devicon-oracle-original colored',
        type: [TYPE.Database],
        exp: {
          val: 2,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'jasmine',
        icon: 'devicon-jasmine-plain colored',
        type: [TYPE.Testing],
        exp: {
          val: 2,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'Windows',
        icon: 'devicon-windows8-original colored',
        type: [TYPE.OS],
        exp: {
          val: 10,
          proxy: APPROXIMATION.PLUS
        }

      }, {
        name: 'Apple',
        icon: 'devicon-apple-original colored',
        type: [TYPE.OS],
        exp: {
          val: 4,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'React',
        icon: 'devicon-react-original colored',
        type: [TYPE.UI],
        exp: {
          val: 1,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'slack',
        icon: 'devicon-slack-plain colored',
        type: [TYPE.Communication]
      }, {
        name: 'Sass',
        icon: 'devicon-sass-original colored',
        exp: {
          val: 2,
          proxy: APPROXIMATION.MINUS
        },
        type: [TYPE.UI]
      }, {
        name: 'Bower',
        icon: 'devicon-bower-plain colored',
        type: [TYPE.Package_Management],
        exp: {
          val: 2,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'd3js',
        icon: 'devicon-d3js-plain colored',
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        },
        type: [TYPE.UI]
      }, {
        name: 'node',
        icon: 'devicon-nodejs-plain colored',
        type: [TYPE.Backend],
        exp: {
          val: 2,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'linux',
        icon: 'devicon-linux-plain colored',
        type: [TYPE.OS ]
      }, {
        name: 'mongodb',
        icon: 'devicon-mongodb-plain colored',
        type: [TYPE.Database ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'express',
        icon: 'devicon-express-original colored',
        type: [TYPE.Backend ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'typescript',
        icon: 'devicon-typescript-plain colored',
        type: [TYPE.UI ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'eclipse',
        icon: 'eclipse-icon',
        type: [TYPE.IDE ],
        exp: {
          val: 3,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'mysql',
        icon: 'devicon-mysql-plain colored',
        type: [TYPE.Database],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'protractor',
        icon: 'devicon-protractor-plain colored',
        type: [TYPE.Testing ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }

      }, {
        name: 'php',
        icon: 'devicon-php-plain colored',
        type: [TYPE.Backend],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'phpstorm',
        icon: 'devicon-phpstorm-plain colored',
        type: [TYPE.IDE ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'svn',
        icon: 'svn-icon',
        type: [TYPE.Source_Control ],
        exp: {
          val: 2,
          proxy: APPROXIMATION.PLUS
        }
      }, {
        name: 'cvs',
        icon: 'cvs-icon',
        type: [TYPE.Source_Control ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'Rxjs',
        icon: 'rx-js-icon',
        type: [TYPE.UI ],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }, {
        name: 'firebase',
        icon: 'firebase-icon',
        type: [TYPE.Backend, TYPE.Database],
        exp: {
          val: 1,
          proxy: APPROXIMATION.MINUS
        }
      }];


    this._navSrvc.scrollFinishEventAnnounced$.subscribe((anchor: string) => {
      if(anchor === "#skills") {
        const grid = this.elem.nativeElement.querySelector('.skill-grid');
        const iso = Isotope.data(grid);

        iso.shuffle();
      }

    });
  }


  toggleInFilterSet(type) {
    if (!this.filterSet.includes(type)) {
      this.filterSet.push(type);
    } else {
      this.filterSet.splice(this.filterSet.indexOf(type), 1);
    }
  }
  filterSkills(type) {
    if (type !== 'all'){
      this.toggleInFilterSet(type);
    } else {
      this.filterSet.length = 0;
    }
    const grid = this.elem.nativeElement.querySelector('.skill-grid-with-legend .skill-grid');
    const iso = Isotope.data(grid);
    iso.arrange({
      filter: (elem) => {
        const category = elem.getAttribute('data-categories');
        return  this.filterSet.length === 0 ? true : this.filterSet.includes(TYPE[category]);
      }
    });
  }

  sortSkills(sortBy?: string, ascend = true): void {
    const grid = this.elem.nativeElement.querySelector('.skill-grid');
    const iso = Isotope.data(grid);
    iso.arrange({
      sortBy: sortBy,
      sortAscending: ascend
    });

    this.currentSort = sortBy;
  }
}
