import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from "@angular/router";
import {ScrollToModule} from "ng2-scroll-to";
import { NavComponent } from './nav/nav.component';
import { IntroComponent } from './intro/intro.component';
import { SkillsComponent } from './skills/skills.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MatCheckboxModule, MatChipsModule, MatDialogModule, MatExpansionModule, MatListModule, MatSelectModule,
  MatSnackBarModule,
  MatTooltipModule
} from "@angular/material";
import { DemosComponent } from './demos/demos.component';
import { ExperiencePipe } from './experience.pipe';
import { AboutComponent } from './about/about.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import {AngularFireModule} from "angularfire2";
import {environment} from "../environments/environment";
import {ReactiveFormsModule} from "@angular/forms";
import {AngularFireDatabaseModule} from "angularfire2/database";
import { ResumeComponent } from './resume/resume.component';
import {NgxCarouselModule} from "ngx-carousel";
import {Ng2CarouselamosModule} from "ng2-carouselamos";
import { BackgroundImageDirective } from './background-image.directive';
import { VideoPopupComponent } from './demos/video-popup/video-popup.component';
import * as $ from 'jquery';
import {MnFullpageModule} from "ngx-fullpage";
import {Ng2PageScrollModule} from "ng2-page-scroll";
import {NavService} from "./nav/nav.service";
import { MultiSelectComponent } from './multi-select/multi-select.component';
import { ScrollableAnimationComponent } from './scrollable-animation/scrollable-animation.component';
import { NavigationComponent } from './nav/navigation.component';
import { PaginatorComponent } from './demos/paginator/paginator.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { DemoDetailsComponent } from './demos/demo-details/demo-details.component';

const appRoutes: Routes = [
  {
    path: 'home',
    component: IntroComponent,
    data: {
      title: '',
      animation: 'homePage'
    }
  },
  {
    path: 'about',
    component: AboutComponent,
    data: {
      title: 'ABOUT <br/> ME',
      animation: 'aboutPage'
    }

  },
  {
    path: 'skills',
    component: SkillsComponent,
    data: { title: 'MY <br/> SKILLS' }

  },
  {
    path: 'works',
    component: DemosComponent,
    data: { title: 'WORK <br/> EXAMPLES' }

  },
  {
    path: 'contact',
    component: ContactMeComponent,
    data: { title: `CONTACT <br/> ME` }

  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
 // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    IntroComponent,
    SkillsComponent,
    DemosComponent,
    ExperiencePipe,
    AboutComponent,
    ContactMeComponent,
    ResumeComponent,
    BackgroundImageDirective,
    VideoPopupComponent,
    MultiSelectComponent,
    ScrollableAnimationComponent,
    NavigationComponent,
    PaginatorComponent,
    DemoDetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    BrowserAnimationsModule,
    Ng2PageScrollModule,
    MatCheckboxModule,
    MatChipsModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatDialogModule,
    MatExpansionModule,
    MatListModule,
    MatSelectModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    NgxCarouselModule,
    Ng2CarouselamosModule,
    OverlayModule,
    MnFullpageModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [NavService],
  bootstrap: [AppComponent],
  entryComponents: [
    VideoPopupComponent
  ],
})
export class AppModule { }
